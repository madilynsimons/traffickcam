package com.gun0912.tedpicker;

import android.net.Uri;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.gun0912.tedpicker.custom.adapter.BaseRecyclerViewAdapter;
import com.tcamdev.traffickcam.HotelImagePickerActivity;
import com.tcamdev.traffickcam.userstudy.TouchLog;
import com.tcamdev.traffickcam.versions.Version;
import com.tcamdev.traffickcam.view.ImagePopup;

/**
 * Created by TedPark on 16. 2. 20..
 */
public class Adapter_SelectedPhoto extends BaseRecyclerViewAdapter<Uri, Adapter_SelectedPhoto.SelectedPhotoHolder> {

    int closeImageRes;

    ImagePickerActivity imagePickerActivity;

    public Adapter_SelectedPhoto(ImagePickerActivity imagePickerActivity, int closeImageRes) {
        super(imagePickerActivity);
        this.imagePickerActivity = imagePickerActivity;
        this.closeImageRes = closeImageRes;
    }

    @Override
    public void onBindView(SelectedPhotoHolder holder, int position) {

        Uri uri = getItem(position);

        Glide.with(imagePickerActivity)
                .load(uri.toString())
                .into(holder.selected_photo);

        holder.selected_photo.setTag(uri);
        holder.iv_close.setTag(uri);
    }

    @Override
    public SelectedPhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater mInflater = LayoutInflater.from(getContext());
        View view = mInflater.inflate(com.tcamdev.traffickcam.R.layout.picker_list_item_selected_thumbnail, parent, false);
        return new SelectedPhotoHolder(view);
    }

    class SelectedPhotoHolder extends ViewHolder implements OnTouchListener {


        ImageView selected_photo;
        ImageView iv_close;


        public SelectedPhotoHolder(View itemView) {
            super(itemView);
            selected_photo = (ImageView) itemView.findViewById(com.tcamdev.traffickcam.R.id.selected_photo);
            if (Version.getInstance().getVersion() == 2) {
                selected_photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        HotelImagePickerActivity hotelImagePickerActivity = (HotelImagePickerActivity) imagePickerActivity;
                        hotelImagePickerActivity.tcGraphics.editImageLabels((Uri) selected_photo.getTag());
                    }
                });
            } else {
                selected_photo.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view){
                        HotelImagePickerActivity hotelImagePickerActivity = (HotelImagePickerActivity) imagePickerActivity;
                        ImagePopup.show(hotelImagePickerActivity, (Uri)selected_photo.getTag());
                    }
                });
            }
            iv_close = (ImageView) itemView.findViewById(com.tcamdev.traffickcam.R.id.iv_close);
            iv_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri uri = (Uri) view.getTag();
                    imagePickerActivity.removeImage(uri);
                }
            });
            enableTouchLogging();
        }

        void enableTouchLogging(){
            selected_photo.setOnTouchListener(this);
            iv_close.setOnTouchListener(this);
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            HotelImagePickerActivity hotelImagePickerActivity = (HotelImagePickerActivity) imagePickerActivity;
            TouchLog.logTouch(hotelImagePickerActivity, v, event);
            //if(event.getAction() == MotionEvent.ACTION_UP) v.performClick();
            return false;
        }
    }

}
