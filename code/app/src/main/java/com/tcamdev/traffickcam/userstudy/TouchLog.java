package com.tcamdev.traffickcam.userstudy;

import android.app.Activity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.google.gson.JsonObject;
import com.tcamdev.traffickcam.util.CurrentTime;

public class TouchLog {

  private static TouchLog touchLog;
  private StringBuffer buffer;

  private TouchLog(){
    Log.e("init", "buffer initialized");
    this.buffer = new StringBuffer();
  }

  private void log(Activity activity, View view, MotionEvent motionEvent){
    JsonObject touch_obj = new JsonObject();

    touch_obj.addProperty("time", CurrentTime.getTime());

    touch_obj.addProperty("activity", activity.getClass().getSimpleName());
    touch_obj.addProperty("view", view.getContentDescription().toString());

    JsonObject motion_event_obj = new JsonObject();
    motion_event_obj.addProperty("MotionEvent", MotionEvent.actionToString(motionEvent.getAction()));
    motion_event_obj.addProperty("X", motionEvent.getX());
    motion_event_obj.addProperty("Y", motionEvent.getY());

    touch_obj.add("touch", motion_event_obj);

    buffer.append(touch_obj.toString());
    buffer.append(System.lineSeparator());
  }

  public static void logTouch(Activity activity, View view, MotionEvent motionEvent){
    Log.e("logging_touch", "logging_touch");
    if(touchLog == null){
      touchLog = new TouchLog();
      touchLog.buffer = new StringBuffer();
    }
    touchLog.log(activity, view, motionEvent);
  }

  public static TouchLog getInstance(){
    if(touchLog == null){
      touchLog = new TouchLog();
      touchLog.buffer = new StringBuffer();
    }
    return touchLog;
  }

  public static String getLogs(){
    return touchLog.buffer.toString();
  }

  public static void clearLog(){
    touchLog.buffer.delete(0, touchLog.buffer.length());
  }

}
