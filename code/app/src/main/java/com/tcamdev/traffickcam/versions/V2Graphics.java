package com.tcamdev.traffickcam.versions;

import android.app.AlertDialog;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import com.tcamdev.traffickcam.HotelImagePickerActivity;
import com.tcamdev.traffickcam.R;
import com.tcamdev.traffickcam.userstudy.TouchLog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class V2Graphics implements OnTouchListener {

  // current activity
  HotelImagePickerActivity activity;

  // pop-up used in version 2 to label photos
  AlertDialog label_photo_popup;

  // last photo taken - shows on pop-up
  ImageView current_image_preview;

  // checkboxes in pop-up
  CheckBox label_photo_popup_checkboxes[];

  // edit text in pop-up so the user can add items
  EditText user_added_hotel_item_edit_text;

  // list of added items for each photo selected
  Map<Uri, String> user_added_hotel_items;

  // checkbox states for each photo selected
  Map<Uri,ArrayList<Boolean>> image_checkbox_states;

  // last photo taken
  Uri newest_uri;

  // items that can be checked
  List<String> items;

  // confirm labels button
  Button confirm_button;

  public V2Graphics(HotelImagePickerActivity activity) {
    this.activity = activity;
    initLabelPopup();
    user_added_hotel_items = new HashMap<>();
    image_checkbox_states  = new HashMap<>();

    String[] item_arr = {"bed", "lamp", "desk", "chair", "couch", "art", "sink", "toilet", "other"};
    items = Arrays.asList(item_arr);
  }

  public void addImage(final Uri uri) {
    newest_uri = uri;
    editImageLabels(uri);
  }

  private void initLabelPopup(){

    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
    LayoutInflater factory = LayoutInflater.from(activity);
    View view = factory.inflate(R.layout.alert_dialog, null);

    initCheckboxes(view);

    current_image_preview = view.findViewById(R.id.dialog_imageview);
    current_image_preview.setVisibility(View.INVISIBLE);
    user_added_hotel_item_edit_text = view.findViewById(R.id.Other);
    user_added_hotel_item_edit_text.setEnabled(false);

    confirm_button = view.findViewById(R.id.confirm_button);
    confirm_button.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        label_photo_popup.dismiss();
        current_image_preview.setVisibility(View.INVISIBLE);
        saveImageLabels();
      }
    });
    confirm_button.setOnTouchListener(this);

    builder.setTitle("Are any of these items in this photo?")
        .setView(view);

    // create the dialog itself
    label_photo_popup = builder.create();

    enableTouchLogging(view);
  }

  void enableTouchLogging(View view){
    view.findViewById(R.id.checkbox_desk).setOnTouchListener(this);
    view.findViewById(R.id.checkbox_bed).setOnTouchListener(this);
    view.findViewById(R.id.checkbox_couch).setOnTouchListener(this);
    view.findViewById(R.id.checkbox_sink).setOnTouchListener(this);
    view.findViewById(R.id.checkbox_chair).setOnTouchListener(this);
    view.findViewById(R.id.checkbox_art).setOnTouchListener(this);
    view.findViewById(R.id.checkbox_lamp).setOnTouchListener(this);
    view.findViewById(R.id.checkbox_toilet).setOnTouchListener(this);
    view.findViewById(R.id.checkbox_other).setOnTouchListener(this);
    view.findViewById(R.id.Other).setOnTouchListener(this);
  }

  private void initCheckboxes(View view){
    label_photo_popup_checkboxes = new CheckBox[9];
    label_photo_popup_checkboxes[0] = view.findViewById(R.id.checkbox_bed);
    label_photo_popup_checkboxes[1] = view.findViewById(R.id.checkbox_lamp);
    label_photo_popup_checkboxes[2] = view.findViewById(R.id.checkbox_desk);
    label_photo_popup_checkboxes[3] = view.findViewById(R.id.checkbox_chair);
    label_photo_popup_checkboxes[4] = view.findViewById(R.id.checkbox_couch);
    label_photo_popup_checkboxes[5] = view.findViewById(R.id.checkbox_art);
    label_photo_popup_checkboxes[6] = view.findViewById(R.id.checkbox_sink);
    label_photo_popup_checkboxes[7] = view.findViewById(R.id.checkbox_toilet);
    label_photo_popup_checkboxes[8] = view.findViewById(R.id.checkbox_other);
  }

  // checks if image in pop-up is labeled
  private boolean imageIsLabeled() {
    boolean isLabeled = false;
    int checkBoxCount = label_photo_popup_checkboxes.length;
    for (int i = 0; i < checkBoxCount && isLabeled != true; ++i)
      if (label_photo_popup_checkboxes[i].isChecked()) isLabeled = true;
    return isLabeled;
  }

  // Enables or disables the buttons on the image classification pop-up
  private void enablePopupButton(final int BUTTON, boolean labelCheck) {
    label_photo_popup.getButton(BUTTON)
        .setEnabled(labelCheck);
  }

  // saves what items are in each photo according to the user
  private void saveImageLabels(){
    ArrayList<Boolean> checkBoxStates = getCheckBoxStates();
    image_checkbox_states.put(newest_uri, checkBoxStates);
    if (label_photo_popup_checkboxes[8].isChecked())
      user_added_hotel_items.put(newest_uri, user_added_hotel_item_edit_text.getText().toString());
    saveImageData(newest_uri);
  }

   // retrieves checkbox states
  private ArrayList<Boolean> getCheckBoxStates() {
    ArrayList<Boolean> checkBoxStates = new ArrayList<>();
    for (CheckBox box : label_photo_popup_checkboxes) {
      if (box.isChecked())
        checkBoxStates.add(true);
      else
        checkBoxStates.add(false);
    }
    return checkBoxStates;
  }

  public void editImageLabels(Uri uri){

    newest_uri = uri;
    label_photo_popup.show();
    if(image_checkbox_states.containsKey(newest_uri))
    {
      ArrayList<Boolean> checkBoxStates = image_checkbox_states.get(newest_uri);
      for (int curr = 0; curr < label_photo_popup_checkboxes.length; curr++) {
        if (checkBoxStates.get(curr))
          label_photo_popup_checkboxes[curr].setChecked(true);
        else
          label_photo_popup_checkboxes[curr].setChecked(false);
      }
      user_added_hotel_item_edit_text.setText(user_added_hotel_items.get(newest_uri));
      enablePopupButton(AlertDialog.BUTTON_POSITIVE, true);
    }
    else
      {
      for(int i = 0; i < label_photo_popup_checkboxes.length; i++)
        label_photo_popup_checkboxes[i].setChecked(false);
      enablePopupButton(AlertDialog.BUTTON_POSITIVE, false);
    }

    current_image_preview.setImageURI(uri);
    int orientation = 0;
    try{
      assert uri != null;
      orientation = activity.getImageRotation(activity, uri);
    } catch(Exception e){
      e.printStackTrace();
    }
    current_image_preview.setRotation(orientation);
    current_image_preview.setVisibility(View.VISIBLE);

  }

  private void saveImageData(Uri uri){
    ArrayList<Boolean> checkBoxStates = image_checkbox_states.get(uri);
    ArrayList<String> uri_items = new ArrayList<>();

    uri_items.add(user_added_hotel_items.get(uri));

    for(int i = 0; i < items.size(); i++){
      if(checkBoxStates.get(i)) uri_items.add(items.get(i));
    }

    if(activity.getImageData().containsImage(uri)){
      activity.getImageData().editImage(uri, uri_items);
    } else {
      activity.getImageData().addImage(uri, uri_items);
    }
  }

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    TouchLog.logTouch(activity, v, event);
    if(event.getAction() == MotionEvent.ACTION_UP) {}//v.performClick();
    else if(event.getAction() == MotionEvent.ACTION_DOWN){
      for(int i = 0; i < label_photo_popup_checkboxes.length - 1; i++){
        CheckBox box = label_photo_popup_checkboxes[i];
        if(v == box){
          //box.setChecked(!box.isChecked());
          if(!box.isChecked() && !imageIsLabeled()){
            enablePopupButton(AlertDialog.BUTTON_POSITIVE, false);
          }else{
            enablePopupButton(AlertDialog.BUTTON_POSITIVE, true);
          }
          return false;
        }
      }
      if(v == label_photo_popup_checkboxes[8]){
        CheckBox box = label_photo_popup_checkboxes[8];
        //box.setChecked(!box.isChecked());
        if(box.isChecked()) {
          user_added_hotel_item_edit_text.setEnabled(true);
          enablePopupButton(AlertDialog.BUTTON_POSITIVE, true);
        }
        else
          user_added_hotel_item_edit_text.setEnabled(false);
        if (!imageIsLabeled())
          enablePopupButton(AlertDialog.BUTTON_POSITIVE,false);
        return false;
      }
    }
    return false;
  }
}
