package com.tcamdev.traffickcam.versions;

import android.net.Uri;
import com.tcamdev.traffickcam.HotelImagePickerActivity;
import com.tcamdev.traffickcam.util.ImageData.TCImage;
import com.tcamdev.traffickcam.view.FotoapparatFragment;

public class TCGraphics {

  private V1Graphics v1_graphics;
  private V2Graphics v2_graphics;
  private V3Graphics v3_graphics;
  HotelImagePickerActivity activity;

  public TCGraphics(HotelImagePickerActivity activity) {
    this.activity = activity;
    switch(Version.getInstance().getVersion()){
      case 1:
        v1_graphics = new V1Graphics(activity);
        break;
      case 2:
        v2_graphics = new V2Graphics(activity);
        break;
      case 3:
        v3_graphics = new V3Graphics(activity);
        break;
    }
  }

  public void addImage(final Uri uri){
    switch(Version.getInstance().getVersion()){
      case 1:
        v1_graphics.addImage(uri);
        break;
      case 2:
        v2_graphics.addImage(uri);
        break;
      case 3:
        v3_graphics.addImage(uri);
        break;
    }
  }

  public void editImageLabels(Uri uri){
    if(Version.getInstance().getVersion() == 2){
      v2_graphics.editImageLabels(uri);
    }
  }

  public Object getGraphics(){
    switch(Version.getInstance().getVersion()){
      case 1:
        return v1_graphics;
      case 2:
        return v2_graphics;
      case 3:
        return v3_graphics;
    }
    return new Object();
  }

  private String getItem(Uri uri){
    if(Version.getInstance().getVersion() == 3){

      TCImage image;
      image = activity.getImageData().getImage(uri);

      if(image != null){
        String item = image.getStencilItem();
        if(item != null) return item;
      }
    }
    return "";
  }

  public void setCamera(FotoapparatFragment cf){
    if(Version.getInstance().getVersion() == 3){
      v3_graphics.initStencilPicker(cf);
    }
  }

}
