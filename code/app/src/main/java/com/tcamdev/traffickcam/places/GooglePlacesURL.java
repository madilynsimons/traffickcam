package com.tcamdev.traffickcam.places;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GooglePlacesURL {

  private static GooglePlacesURL instance;
  private static String location_url;

  public static GooglePlacesURL getInstance(){
    if (instance == null)
      instance = new GooglePlacesURL();
    return instance;
  }

  public static String getURL(){ return location_url; }

  public static String setLocationURL(
      String keywords, String nearbyPlace, double latitude, double longitude, int PROXIMITY_RADIUS)
  {
    StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
    googlePlaceUrl.append("location="+latitude+","+longitude);
    googlePlaceUrl.append("&radius="+PROXIMITY_RADIUS);
    googlePlaceUrl.append("&type="+nearbyPlace);
    googlePlaceUrl.append("&sensor=true");
    googlePlaceUrl.append("&keyword=" + keywords);
    googlePlaceUrl.append("&key="+"AIzaSyDlTwcKRDanIkhKboghxUS22O79AlF0kfM");

    location_url = googlePlaceUrl.toString();
    return location_url;
  }

  public static String readUrl() throws IOException
  {
    String myUrl = location_url;

    String data = "";
    InputStream inputStream = null;
    HttpURLConnection urlConnection = null;

    try {
      URL url = new URL(myUrl);
      urlConnection=(HttpURLConnection) url.openConnection();
      urlConnection.connect();

      inputStream = urlConnection.getInputStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
      StringBuffer sb = new StringBuffer();

      String line = "";
      while((line = br.readLine()) != null)
      {
        sb.append(line);
      }

      data = sb.toString();
      br.close();

    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    finally {
      if(inputStream != null) inputStream.close();
      if(urlConnection!=null) urlConnection.disconnect();
    }

    return data;
  }

}
