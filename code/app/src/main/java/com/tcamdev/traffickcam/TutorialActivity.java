package com.tcamdev.traffickcam;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.tcamdev.traffickcam.userstudy.TouchLog;
import com.tcamdev.traffickcam.versions.Version;

public class TutorialActivity extends AppCompatActivity implements OnTouchListener {

  private static int version = -1;

  /**
   * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
   * sections. We use a {@link FragmentPagerAdapter} derivative, which will keep every loaded
   * fragment in memory. If this becomes too memory intensive, it may be best to switch to a {@link
   * android.support.v4.app.FragmentStatePagerAdapter}.
   */
  private SectionsPagerAdapter mSectionsPagerAdapter;

  /**
   * The {@link ViewPager} that will host the section contents.
   */
  private ViewPager mViewPager;
  static Activity activity;
  static String parent;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_tutorial);
    activity = this;

    parent = getIntent().getStringExtra("parent");

    version = Version.getInstance().getVersion();

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

    mViewPager = findViewById(R.id.container);
    mViewPager.setAdapter(mSectionsPagerAdapter);

    TabLayout tabLayout = findViewById(R.id.tabLayout);
    tabLayout.setupWithViewPager(mViewPager, true);
    tabLayout.setOnTouchListener(this);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_tutorial, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.action_settings)
      return true;
    return super.onOptionsItemSelected(item);
  }

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    TouchLog.logTouch(this, v, event);
    return false;
  }

  public static class TutorialPictureFragment extends Fragment{

    private static final String ARG_SECTION_NUMBER = "section_number";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
      View rootView = inflater.inflate(R.layout.fragment_tutorial, container, false);
      ImageView imageView = rootView.findViewById(R.id.image);
      Button button = rootView.findViewById(R.id.button);
      button.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {

          if(parent.equals(HotelImagePickerActivity.activity_tag)){
            activity.finish();

          }else{
            Intent intent = new Intent(activity, HotelImagePickerActivity.class);
            startActivity(intent);
            activity.finish();
          }

        }
      });
      button.setVisibility(View.INVISIBLE);
      int position = getArguments().getInt(ARG_SECTION_NUMBER);
      switch(position){
        case 0:
          imageView.setImageResource(R.drawable.tutorial1);
          button.setVisibility(View.INVISIBLE);
          break;
        case 1:
          imageView.setImageResource(R.drawable.tutorial2);
          button.setVisibility(View.INVISIBLE);
          break;
        case 2:
          if(version == 1) {
            imageView.setImageResource(R.drawable.tutorial3a);
            button.setVisibility(View.VISIBLE);
          } else if (version == 2) {
            imageView.setImageResource(R.drawable.tutorial3b);
            button.setVisibility(View.VISIBLE);
          } else if (version == 3){
            imageView.setImageResource(R.drawable.tutorial3c);
            button.setVisibility(View.VISIBLE);
          }
          break;
      }
      enableTouchLogging(rootView);
      return rootView;
    }

    static boolean onTouchStatic(View v, MotionEvent event) {
      TouchLog.logTouch(activity, v, event);
      return false;
    }

    static void enableTouchLogging(View view){
      view.findViewById(R.id.image).setOnTouchListener(new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
          return onTouchStatic(v, event);
        }
      });
      view.findViewById(R.id.button).setOnTouchListener(new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
          return onTouchStatic(v, event);
        }
      });
    }
  }


  public class SectionsPagerAdapter extends FragmentPagerAdapter {

    SectionsPagerAdapter(FragmentManager fm) {
      super(fm);
    }

    @Override
    public Fragment getItem(int position) {
      TutorialPictureFragment fragment = new TutorialPictureFragment();
      Bundle args = new Bundle();
      args.putInt(TutorialPictureFragment.ARG_SECTION_NUMBER, position);
      fragment.setArguments(args);
      return fragment;
    }

    @Override
    public int getCount() {
      return 3;
    }
  }
}
