package com.tcamdev.traffickcam.versions;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.tcamdev.traffickcam.R;
import com.tcamdev.traffickcam.userstudy.TouchLog;
import com.tcamdev.traffickcam.view.FotoapparatFragment;
import java.util.List;


public class StencilAdapter extends RecyclerView.Adapter<StencilAdapter.ViewHolder> {

    private RecyclerView parentRecycler;
    private List<Stencil> data;
    private FotoapparatFragment camera;


    public StencilAdapter(List<Stencil> data, FotoapparatFragment camera) {
        this.data = data;
        this.camera = camera;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        parentRecycler = recyclerView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_hotel_btn, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Stencil stencil = data.get(position);
        Glide.with(holder.itemView.getContext())
                .load(stencil.getIcon())
                .into(holder.imageView);
        //holder.textView.setText(stencil.getInstructions());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public RecyclerView getParentRecycler() { return parentRecycler; }

    class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener, OnTouchListener {

        private ImageView imageView;
        private TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.hotel_item);
            //textView = (TextView) itemView.findViewById(R.id.city_name);
            imageView.setColorFilter(ContextCompat.getColor(parentRecycler.getContext(),
                    R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
            imageView.setAlpha(.62f);
            imageView.setOnClickListener(this);
            imageView.setOnTouchListener(this);
        }

        /*public void showText() {
            int parentHeight = ((View) imageView.getParent()).getHeight();
            float scale = (parentHeight - textView.getHeight()) / (float) imageView.getHeight();
            imageView.setPivotX(imageView.getWidth() * 0.5f);
            imageView.setPivotY(0);
            imageView.animate().scaleX(scale)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            textView.setVisibility(View.VISIBLE);
                            imageView.setColorFilter(Color.BLACK);
                        }
                    })
                    .scaleY(scale).setDuration(200)
                    .start();
        }*/

        /*public void hideText() {
            imageView.setColorFilter(ContextCompat.getColor(imageView.getContext(), R.color.grayIconTint));
            textView.setVisibility(View.INVISIBLE);
            imageView.animate().scaleX(1f).scaleY(1f)
                    .setDuration(200)
                    .start();
        }*/

        @Override
        public void onClick(View v) {

           parentRecycler.smoothScrollToPosition(getAdapterPosition());
            if (v.getTag() == "current") {
                Log.e("view_tag", "current");
                camera.onClick(v);
               // camera.initImageFile();
              //  camera.takePicture();
            }else{
                Log.e("view_tag", "not current");
            }

        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if(v.getTag() == "current")
                TouchLog.logTouch(camera.getActivity(), v, event);
            return false;
        }
    }

}



