package com.tcamdev.traffickcam.util;

public class UploadState {

  private static int upload_tasks = 0;

  public final static int UPLOADING = 1;
  public final static int NOT_UPLOADING = 0;

  private static int CurrentState = NOT_UPLOADING;

  public static int getCurrentState(){
    return CurrentState;
  }

  public static void addUploadTask(int n){
    upload_tasks += n;
    CurrentState = UPLOADING;
  }

  public static void finishUpload(){
    upload_tasks--;
    if(upload_tasks <= 0){
      upload_tasks = 0;
      CurrentState = NOT_UPLOADING;
    }else{
      CurrentState = UPLOADING;
    }
  }

}
