package com.tcamdev.traffickcam.userstudy;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.firebase.storage.UploadTask.TaskSnapshot;
import com.google.gson.JsonObject;
import com.tcamdev.traffickcam.SubmitDataActivity;
import com.tcamdev.traffickcam.places.UserHotel;
import com.tcamdev.traffickcam.util.CurrentTime;
import com.tcamdev.traffickcam.util.UploadState;
import com.tcamdev.traffickcam.versions.Version;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Logger {

  private static SubmitDataActivity activity;

  // Firebase
  private static FirebaseStorage storage;
  private static StorageReference session_reference;
  private static StorageReference images_reference;
  private static StorageReference trash_reference;


  public static void uploadData(SubmitDataActivity submitDataActivity,
      String images_json, ArrayList<Uri> image_uris,
      ArrayList<Uri> deleted_image_uri,
      UserHotel user_hotel) {

    activity = submitDataActivity;
    initFirebase();
    // progress
    int elements_to_upload = 3;
    if(image_uris != null) elements_to_upload += image_uris.size();
    if(deleted_image_uri != null) elements_to_upload += deleted_image_uri.size();

    UploadState.addUploadTask(elements_to_upload);

    // initialize JSON files
    String images_json_name = "images.json";
    String session_info_json_name = "session_info.json";
    String touch_log_json_name = "touches_log.json";

    writeJsonFile(activity, images_json_name, images_json);

    String session_info_json = getSessionInfo(user_hotel);
    writeJsonFile(activity, session_info_json_name, session_info_json);

    String touch_log_contents = TouchLog.getLogs();
    writeJsonFile(activity, touch_log_json_name, touch_log_contents);
    TouchLog.clearLog();

    if(image_uris != null)
      writeImageFile(image_uris, images_reference);
    if(deleted_image_uri != null)
      writeImageFile(deleted_image_uri, trash_reference);

    activity.finishUploadSuccess();

  }

  private static void initFirebase(){
    storage = FirebaseStorage.getInstance();

    StorageReference root_storage_reference = storage.getReference();

    String year = CurrentTime.year();
    String month = CurrentTime.month();
    String day = CurrentTime.day();

    StorageReference year_reference = root_storage_reference.child(year);
    StorageReference month_reference = year_reference.child(month);
    StorageReference day_reference = month_reference.child(day);

    String session_file_name = CurrentTime.getTime();

    session_reference = day_reference.child(session_file_name);

    images_reference = session_reference.child("images");
    trash_reference = session_reference.child("trash");
  }

  private static String getSessionInfo(UserHotel hotel){
    JsonObject session_json = new JsonObject();
    session_json.addProperty("user_id", Version.getInstance().getUserID());
    session_json.addProperty("version", Version.getInstance().getVersion());

    JsonObject hotel_json = new JsonObject();
    hotel_json.addProperty("name", hotel.name());
    hotel_json.addProperty("address", hotel.address());
    hotel_json.addProperty("room", hotel.roomNumber());
    session_json.add("hotel", hotel_json);

    return session_json.toString();
  }

  private static void writeJsonFile(Activity activity, String file_name, String file_contents){
    BufferedWriter bw;
    FileWriter fw;
    String name = activity.getExternalFilesDir(null) + file_name;
    try{
      fw = new FileWriter(name);
      bw = new BufferedWriter(fw);
      bw.write(file_contents);
      bw.close();
    } catch (IOException e) {
      e.printStackTrace();
     return;
    }

    File file = new File(name);
    StorageReference ref = session_reference.child(file_name);
    UploadTask upload_task = ref.putFile(Uri.fromFile(file));
    upload_task.addOnCompleteListener(new OnCompleteListener<TaskSnapshot>() {
      @Override
      public void onComplete(@NonNull Task<TaskSnapshot> task) {
        UploadState.finishUpload();
      }
    });
  }

  private static void writeImageFile(ArrayList<Uri> images, StorageReference reference){
    for(Uri img : images){
      StorageReference ref = reference.child(img.getLastPathSegment());
      UploadTask upload_task = ref.putFile(img);
      upload_task.addOnCompleteListener(new OnCompleteListener<TaskSnapshot>() {
        @Override
        public void onComplete(@NonNull Task<TaskSnapshot> task) {
          UploadState.finishUpload();
        }
      });
    }
  }

}
