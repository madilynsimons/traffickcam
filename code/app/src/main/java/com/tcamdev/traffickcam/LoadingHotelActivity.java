package com.tcamdev.traffickcam;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import com.tcamdev.traffickcam.userstudy.TouchLog;

public class LoadingHotelActivity extends AppCompatActivity implements OnTouchListener {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_loading_hotel);

    TextView loading_txt_view = findViewById(R.id.loading_txt);
    loading_txt_view.setText("Searching nearby hotels...");
  }

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    TouchLog.logTouch(this,v, event);
    //if(event.getAction() == MotionEvent.ACTION_UP) v.performClick();
    return false;
  }
}
