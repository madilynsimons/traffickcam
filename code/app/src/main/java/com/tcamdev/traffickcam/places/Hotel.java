package com.tcamdev.traffickcam.places;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.tcamdev.traffickcam.SubmitDataActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Hotel  implements
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener{

  // location data
  private boolean NO_HOTELS_FOUND = false;
  private final double DEFAULT_LONGITUDE = 39.9816806;
  private final double DEFAULT_LATITUDE = -75.158515;
  private double longitude = DEFAULT_LONGITUDE;
  private double latitude = DEFAULT_LATITUDE;
  private final int PROXIMITY_RADIUS = 10000;

  // google places data
  private GoogleApiClient client;
  private String googlePlacesData;

  // hotel data
  private ArrayList<String> hotelNames = new ArrayList<>();
  private ArrayList<String> hotelAddresses = new ArrayList<>();

  // app data
  SubmitDataActivity activity;

  public Hotel(SubmitDataActivity activity){
    this.activity = activity;
    buildClient();
  }

  public void findNearbyHotels()
  {
    if(client != null){
      String keywords = ""; String type = "lodging";
      GooglePlacesURL.getInstance().setLocationURL(
          keywords, type, latitude, longitude, PROXIMITY_RADIUS);
      new GetHotelsTask().getInstance().execute();
    }
  }

  void setHotelData(List<HashMap<String, String>> hotels) {
    if (hotelNames == null)
      hotelNames = new ArrayList<>();
    else
      hotelNames.clear();

    if (hotelAddresses == null)
      hotelAddresses = new ArrayList<>();
    else
      hotelAddresses.clear();

    for (HashMap<String, String> hotel : hotels) {
      if (hotel.get("place_name") != null && hotel.get("vicinity") != null) {
        hotelNames.add(hotel.get("place_name"));
        hotelAddresses.add(hotel.get("vicinity"));
      }
    }

    if (hotelNames.size() == 0){
      HotelList.noHotelsFound = true;
      NO_HOTELS_FOUND = true;
      ArrayList<String> no_hotels_list = new ArrayList<>();
      String no_hotels = "no hotels found";
      no_hotels_list.add(no_hotels);
      activity.setHotelData(no_hotels_list, no_hotels_list);
    }
    else{
      HotelList.noHotelsFound = false;
      NO_HOTELS_FOUND = false;
      activity.setHotelData(hotelNames, hotelAddresses);
    }
  }

  public boolean noHotelsFound(){ return NO_HOTELS_FOUND; }

  private synchronized void buildClient(){
    client = new GoogleApiClient.Builder(activity).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(
        LocationServices.API).build();
    client.connect();
  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {
    LocationRequest locationRequest;
    locationRequest = new LocationRequest();
    locationRequest.setInterval(100);
    locationRequest.setFastestInterval(1000);
    locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

    LocationCallback locationCallback;
    locationCallback = new LocationCallback(){
      @Override
      public void onLocationResult(LocationResult result){
        if(hotelNames.size() == 0 && result != null)
        {
          Location location = result.getLastLocation();
          if(location != null)
          {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            findNearbyHotels();
          }
        }
      }
    };

    if(ContextCompat
        .checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED)
      getFusedLocationProviderClient(activity).requestLocationUpdates(locationRequest, locationCallback, null);

  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  public class GetHotelsTask extends AsyncTask<Object, String, String> {

    private GetHotelsTask instance;

    public GetHotelsTask getInstance() {
      if (instance == null)
        instance = new GetHotelsTask();
      return instance;
    }

  /**
   *   Downloads data on all nearby hotels
   */
    @Override
    protected String doInBackground(Object... objects)
    {
      try
      {
        Log.i("Google Places Data", "Getting hotel data...");
        Log.i("google url", GooglePlacesURL.getInstance().getURL());
        googlePlacesData = GooglePlacesURL.getInstance().readUrl();
      }catch(Exception e)
      {
        Log.e("Hotel Data Error", "Failed to get hotel data");
      }
      return googlePlacesData;
    }

    @Override
    protected void onPostExecute(String s){
      Log.i("Google Places Data", s);
      DataParser parser = new DataParser();
      List<HashMap<String, String>> hotels = parser.parse(s);
      setHotelData(hotels);
    }
  }

}
