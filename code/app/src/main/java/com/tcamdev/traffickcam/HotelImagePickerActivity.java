
package com.tcamdev.traffickcam;

import android.Manifest;
import android.Manifest.permission;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.gun0912.tedpicker.ImagePickerActivity;
import com.tcamdev.traffickcam.userstudy.TouchLog;
import com.tcamdev.traffickcam.userstudy.User;
import com.tcamdev.traffickcam.util.ImageData;
import com.tcamdev.traffickcam.util.UploadState;
import com.tcamdev.traffickcam.versions.TCGraphics;
import com.tcamdev.traffickcam.view.FotoapparatFragment;
import java.util.ArrayList;

public class HotelImagePickerActivity
  extends ImagePickerActivity implements OnTouchListener, OnRequestPermissionsResultCallback {

  final static int SUBMIT_DATA_ACTIVITY = 1;

  /** Graphics class for TC actions **/
  public TCGraphics tcGraphics;

  /** Uri file for latest selected photo **/
  public Uri newestUri = null;

  /** Records number of clicks on the title text view **/
  int titleClicks;

  /** Camera Fragment **/
  FotoapparatFragment mCamera = null;

  /** holds images and data **/
  ImageData imageData = new ImageData();

  /** Button to start SubmitData activity **/
  ImageButton submitDataButton = null;

  /** Image Data tag **/
  static String IMAGES_JSON = "IMAGES_JSON";
  static String ADDED_IMAGES_URI = "ADDED_IMAGES_URI";
  static String DELETED_IMAGES_URI = "DELETED_IMAGES_URI";

  public final static String activity_tag = "HOTEL_IMAGE_PICKER_ACTIVITY" ;

  public ProgressBar uploadProgressView = null;

  /**
   * @param savedInstanceState
   *
   * Called when an instance of ImagePickerActivity is first run.
   * Calls to initialize most views and user-side components of the activity
   * Also checks that TraffickCam has permission to read external storage and
   * requests permission if that permission is not currently granted
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setupFromSavedInstanceState(savedInstanceState);
    setContentView(R.layout.hotel_image_picker_activity);

    // for FireBase
    User.signIn(this);

    // view
    setUpMainViewRoot();
    setTitle(configuration.getToolbarTitleRes());
    setupTabs();
    setSelectedPhotoRecyclerView();
    setUpSubmitButton();
    setUpInfoButton();
    tcGraphics = new TCGraphics(this);

    enableTouchLogging();

    // check for permissions
    // ask for permission if not granted

    boolean storage_permission = ( ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    boolean camera_permission = ( ContextCompat.checkSelfPermission(this, permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    boolean location_permission = ( ContextCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);

    if(!storage_permission || !camera_permission || !location_permission){
      ActivityCompat.requestPermissions(this, new String[]{
          permission.READ_EXTERNAL_STORAGE,
          permission.CAMERA,
          permission.ACCESS_FINE_LOCATION
      }, 1);
    }

  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    for(int result : grantResults){
      if(result == PackageManager.PERMISSION_DENIED){
        AlertDialog.Builder builder = new Builder(this);
        builder.setTitle("Warning");
        builder.setMessage("TraffickCam requires certain permissions in order to work correctly.  Please check TraffickCam's permissions under your phone's settings.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
          }
        });
        builder.create().show();
        return;
      }
    }
  }

  void enableTouchLogging(){
    findViewById(R.id.stencil_view).setOnTouchListener(this);
    findViewById(R.id.action_done_btn).setOnTouchListener(this);
    findViewById(R.id.rc_selected_photos).setOnTouchListener(this);
    findViewById(R.id.info_button).setOnTouchListener(this);
  }

  void setUpInfoButton(){
    ImageView info_button = findViewById(R.id.info_button);
    info_button.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        // open tutorial activity
        Intent intent = new Intent(getApplicationContext(), TutorialActivity.class);
        intent.putExtra("parent", activity_tag);
        startActivity(intent);
      }
    });
  }

  void setUpSubmitButton(){
    submitDataButton = findViewById(R.id.action_done_btn);
    submitDataButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        updatePictures();
      }
    });

  }

  public ImageData getImageData(){return imageData;}

  public void setCamera(FotoapparatFragment cf){
    Log.e("HIPA", "Camera set");
    mCamera = cf;
    tcGraphics.setCamera(cf);
  }

  private void setUpMainViewRoot() {
    uploadProgressView = findViewById(R.id.upload_progress);
    uploadProgressView.setVisibility(View.INVISIBLE);
    toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    mainViewRoot = findViewById(R.id.view_root);
    nonselectedImagesViewPager = findViewById(R.id.pager);
    tabLayout = findViewById(R.id.tab_layout);
    selectedPhotosTitleTextView = findViewById(R.id.tv_selected_title);
    initRecyclerView();
    selectedImageEmptyView = findViewById(R.id.selected_photos_empty);
    selectedPhotosContainerView = findViewById(R.id.view_selected_photos_container);
    selectedPhotosContainerView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
      @Override
      public boolean onPreDraw() {
        selectedPhotosContainerView.getViewTreeObserver().removeOnPreDrawListener(this);

        int selected_bottom_size = (int) getResources().getDimension(configuration.getSelectedBottomHeight());

        ViewGroup.LayoutParams params = selectedPhotosContainerView.getLayoutParams();
        params.height = selected_bottom_size;
        selectedPhotosContainerView.setLayoutParams(params);
        return true;
      }
    });
    if (configuration.getSelectedBottomColor() > 0) {
      selectedPhotosTitleTextView.setBackgroundColor(ContextCompat.getColor(this, configuration.getSelectedBottomColor()));
      selectedImageEmptyView.setTextColor(ContextCompat.getColor(this, configuration.getSelectedBottomColor()));
    }
    // start version picker if Traffickcam logo is clicked three times
    titleClicks = 0;
    findViewById(R.id.title_clickable).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        titleClicks++;
        if(titleClicks == 3) {
          Intent intent = new Intent(v.getContext(), VersionPickerActivity.class);
          intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
          v.getContext().startActivity(intent);
          titleClicks = 0;
        }
      }
    });
  }

  private void initRecyclerView(){
    selectedPhotosRecyclerView = findViewById(R.id.rc_selected_photos);
  }

  /**
   *  @param uri
   *
   *  Adds given uri to selected images
   */
  @Override
  public void addImage(final Uri uri) {

    if (selectedImages.size() == configuration.getSelectionLimit()) {
      String text = String.format(getResources().getString(R.string.max_count_msg), configuration.getSelectionLimit());
      Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
      return;
    }

    tcGraphics.addImage(uri);
    newestUri = uri;

    selectedImages.add(uri);
    selectedPhotoAdapter.updateItems(selectedImages);

    if (selectedImages.size() >= 1) {
      selectedImageEmptyView.setVisibility(View.GONE);
    }

    selectedPhotosRecyclerView.smoothScrollToPosition(selectedPhotoAdapter.getItemCount()-1);
  }

  @Override
  public void removeImage(Uri uri){
    super.removeImage(uri);
    imageData.removeImage(uri);
  }

  @Override
  protected void onStart(){
    super.onStart();
    if(mCamera != null){
      mCamera.start();
    }
  }

  @Override
  protected void onStop(){
    super.onStop();
    if(mCamera != null){
      mCamera.stop();
    }
  }

  @Override
  protected void updatePictures(){
    if (selectedImages.size() < 1) {
      String text = String.format(getResources().getString(R.string.min_count_msg));
      Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
      return;
    }

    Log.d("TRANSFERRING_PHOTOS", Integer.toString(imageData.getImages().size()));
    String images_json = imageData.toJson().toString();
    ArrayList<Uri> added_images = imageData.getUris();
    ArrayList<Uri> deleted_images = imageData.getDeletedUris();

    Intent intent = new Intent(HotelImagePickerActivity.this, SubmitDataActivity.class);
    Bundle bundle = new Bundle();
    bundle.putString(IMAGES_JSON, images_json);
    bundle.putParcelableArrayList(ADDED_IMAGES_URI, added_images);
    bundle.putParcelableArrayList(DELETED_IMAGES_URI, deleted_images);
    intent.putExtras(bundle);
    startActivityForResult(intent, SUBMIT_DATA_ACTIVITY);
  }


  void deleteAllData(){
    imageData.clear();
    selectedImages.clear();
    removedImages.clear();
    selectedPhotoAdapter.updateItems(selectedImages);
    selectedPhotosRecyclerView.smoothScrollToPosition(0);
    selectedImageEmptyView.setVisibility(View.VISIBLE);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == SUBMIT_DATA_ACTIVITY) {
      if(resultCode == Activity.RESULT_OK)
      {

        if(UploadState.getCurrentState() == UploadState.UPLOADING){
          uploadProgressView.setVisibility(View.VISIBLE);
          new UploadStateListener().execute((Void) null);
        }else{
          uploadProgressView.setVisibility(View.INVISIBLE);
        }
        deleteAllData();
      }
    }
  }

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    TouchLog.logTouch(this, v, event);
    return false;
  }

  public void finishUpload(){
    if(uploadProgressView != null) uploadProgressView.setVisibility(View.INVISIBLE);
  }

  class UploadStateListener extends AsyncTask <Void, Void, Void>{

    @Override
    protected Void doInBackground(Void... voids) {
      while(true){
        if(UploadState.getCurrentState() == UploadState.NOT_UPLOADING){
          return null;
        }else{
          try {
            Thread.sleep(2000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }

    @Override
    protected void onPostExecute(Void result){
      finishUpload();
    }
  }
}
