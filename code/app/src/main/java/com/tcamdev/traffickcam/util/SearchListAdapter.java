package com.tcamdev.traffickcam.util;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.tcamdev.traffickcam.ChangeHotelActivity;
import com.tcamdev.traffickcam.R;
import java.util.ArrayList;
import java.util.List;

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.SearchItemHolder> implements
    Filterable {

  private ChangeHotelActivity activity;
  private List<SearchItem> search_items;
  private List<SearchItem> search_items_full;

  @NonNull
  @Override
  public SearchItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_item, viewGroup, false);
    return new SearchItemHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull SearchItemHolder searchItemHolder, int i) {
    SearchItem current_item = search_items.get(i);

    searchItemHolder.primary_text_view.setText(current_item.primary_descriptor);
    searchItemHolder.secondary_text_view.setText(current_item.secondary_descriptor);

  }

  @Override
  public int getItemCount() {
    return search_items.size();
  }

  @Override
  public Filter getFilter() {
    return search_item_filter;
  }

  private Filter search_item_filter = new Filter(){

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
      List<SearchItem> filtered_list = new ArrayList<>();

      if(constraint == null || constraint.length() == 0){
        filtered_list.addAll(search_items_full);
      }else {
        String filter_pattern = constraint.toString().toLowerCase().trim();

        for(SearchItem item : search_items_full){
          if(item.getDescription().toLowerCase().contains(filter_pattern)){
            filtered_list.add(item);
          }
        }
      }

      FilterResults results = new FilterResults();
      results.values = filtered_list;
      return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
      search_items.clear();
      search_items.addAll((List) results.values);
      notifyDataSetChanged();
    }
  };

  public class SearchItemHolder extends RecyclerView.ViewHolder implements OnClickListener {
    TextView primary_text_view;
    TextView secondary_text_view;

    SearchItemHolder(final View item_view){
      super(item_view);
      primary_text_view = item_view.findViewById(R.id.primary_text_view);
      secondary_text_view = item_view.findViewById(R.id.secondary_text_view);
      item_view.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      v.setBackgroundColor(Color.LTGRAY);
      activity.setHotel(primary_text_view.getText().toString());
    }
  }

  public SearchListAdapter(ChangeHotelActivity activity, List<SearchItem> search_items){
    this.activity = activity;
    this.search_items = search_items;
    search_items_full = new ArrayList<>(search_items);
  }

}
