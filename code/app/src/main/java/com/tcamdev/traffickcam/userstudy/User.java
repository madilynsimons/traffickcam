package com.tcamdev.traffickcam.userstudy;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.tcamdev.traffickcam.versions.Version;

public class User {

  private static FirebaseAuth mAuth;
  private static String TAG = "User sign in";

  public static void signIn(Activity activity){
    if(mAuth == null){
      mAuth = FirebaseAuth.getInstance();
    }
    mAuth.signInAnonymously()
        .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
          @Override
          public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful()) {
              // Sign in success, update UI with the signed-in user's information
              Log.d(TAG, "signInAnonymously:success");
            } else {
              // If sign in fails, display a message to the user.
              Log.w(TAG, "signInAnonymously:failure", task.getException());
            }
          }
        });
  }

  public static String getID(){
    return Version.getInstance().getUserID();
  }

}
