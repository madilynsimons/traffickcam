package com.tcamdev.traffickcam.util;

import android.graphics.BitmapFactory;
import android.net.Uri;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tcamdev.traffickcam.versions.Version;
import java.util.ArrayList;
import java.util.HashMap;

public class ImageData {

  private HashMap<Uri, DeletedTCImage> deletedImages =  new HashMap<>();

  private HashMap<Uri, TCImage> images = new HashMap<>();

  public ImageData(){}

  // version 1
  public void addImage(Uri uri){
    if(Version.getInstance().getVersion() == 1){
      String time = CurrentTime.getTime();
      TCImage image = new TCImage();
      image.setUri(uri);
      image.setTime(time);
      images.put(uri, image);
    }
  }

  // version 2
  public void addImage(Uri uri, ArrayList<String> items){
    if(Version.getInstance().getVersion() == 2){
      String time = CurrentTime.getTime();
      TCImage image = new TCImage();
      image.setUri(uri);
      image.setTime(time);
      image.setItems(items);
      images.put(uri, image);
    }
  }

  // version 3
  public void addImage(Uri uri, String item, int[] location){
    if(Version.getInstance().getVersion() == 3){
      String time = CurrentTime.getTime();
      TCImage image = new TCImage();
      image.setUri(uri);
      image.setTime(time);
      image.setStencilItem(item);
      image.setStencilLocation(location);
      images.put(uri, image);
    }
  }

  public void removeImage(Uri uri){

    if(images.containsKey(uri)){

      TCImage img = images.get(uri);
      images.remove(uri);
      DeletedTCImage deletedImg = new DeletedTCImage(img);
      deletedImages.put(uri, deletedImg);
    }
  }

  public boolean containsImage(Uri uri){
    return images.containsKey(uri);
  }

  public void editImage(Uri uri, ArrayList<String> items){
    if(images.containsKey(uri)){
      TCImage image_to_edit = images.get(uri);
      image_to_edit.setItems(items);
    }
  }

  public TCImage getImage(Uri uri){
    if(images.containsKey(uri)){
      return images.get(uri);
    }
    return null;
  }

  public ArrayList<TCImage> getImages(){
    ArrayList<TCImage> list = new ArrayList<>(images.size());
    list.addAll(images.values());
    return list;
  }

  public ArrayList<Uri> getUris() {
    ArrayList<Uri> arr = new ArrayList<>(images.size());
    for(TCImage img : images.values()){
      arr.add(img.getUri());
    }
    return arr;
  }

  public ArrayList<Uri> getDeletedUris(){
    ArrayList<Uri> arr = new ArrayList<>(deletedImages.size());
    for(TCImage img : deletedImages.values()){
      arr.add(img.getUri());
    }
    return arr;
  }

  ArrayList<DeletedTCImage> getDeletedImages() {
    ArrayList<DeletedTCImage> list = new ArrayList<>(deletedImages.size());
    list.addAll(deletedImages.values());
    return list;
  }

  public JsonObject toJson(){

    JsonObject imagesJson = new JsonObject();

    JsonArray addedImagesJson = new JsonArray();
    JsonArray deletedImagesJson = new JsonArray();

    int i = 1;
    for(TCImage img : images.values()){
      addedImagesJson.add(img.toJson(i));
      i++;
    }
    i = 1;
    for(DeletedTCImage img: deletedImages.values()){
      deletedImagesJson.add(img.toJson(i));
      i++;
    }

    imagesJson.add("added_images", addedImagesJson);
    imagesJson.add("deleted_images", deletedImagesJson);
    return imagesJson;
  }

  public void clear(){
    deletedImages.clear();
    images.clear();
  }


  public class TCImage{

    // all versions
    Uri uri = null;
    String time = "UNKNOWN_TIME";
    int width = -1;
    int height = -1;

    // v2 only
    ArrayList<String> items = new ArrayList<>();

    // v3 only
    String stencil_item = "UNKNOWN_STENCIL";
    int[] stencil_location = {-1, -1, -1, -1}; // x, y, height, width

    TCImage(){}

    void setUri(Uri uri) {
      if(uri == null) return;

      this.uri = uri;
      BitmapFactory.Options options = new BitmapFactory.Options();
      options.inJustDecodeBounds = true;

      BitmapFactory.decodeFile(uri.getPath(), options);
      width = options.outWidth;
      height = options.outHeight;
    }
    public Uri getUri() { return uri; }
    void setTime(String time) {this.time = time;}
    String getTime() {return time;}
    void setItems(ArrayList<String> items) {this.items = items;}
    ArrayList<String> getItems(){ return items; }
    void setStencilItem(String stencil_item){ this.stencil_item = stencil_item; }
    public String getStencilItem(){return stencil_item; }
    void setStencilLocation(int[] location){this.stencil_location = location; }
    int[] getStencilLocation(){return stencil_location;}
    JsonObject toJson(int picture_id){

      JsonObject imageJson = new JsonObject();

      // file information
      JsonObject fileJson = new JsonObject();
      String file_name;
      if(uri == null) file_name = "FILE_NOT_FOUND";
      else file_name = uri.getLastPathSegment();
      fileJson.addProperty("name", file_name);
      fileJson.addProperty("height", height);
      fileJson.addProperty("width", width);
      imageJson.add("file", fileJson);

      // time
      imageJson.addProperty("time", time);

      // objects v2
      if(Version.getInstance().getVersion() == 2){
        JsonArray objectsJson = new JsonArray();
        for(String item : items){
          objectsJson.add(item);
        }
        imageJson.add("objects", objectsJson);
      }

      // objects v3
      else if(Version.getInstance().getVersion() == 3){
        int[] location = getStencilLocation();
        JsonObject objectJson = new JsonObject();
        objectJson.addProperty("name", stencil_item);
        objectJson.addProperty("x", location[0]);
        objectJson.addProperty("y", location[1]);
        objectJson.addProperty("width", location[2]);
        objectJson.addProperty("height,", location[3]);
        imageJson.add("object", objectJson);
      }

      // final json
      String name = "picture_" + Integer.toString(picture_id);
      JsonObject jsonObject = new JsonObject();
      jsonObject.add(name, imageJson);
      return jsonObject;
    }
  }

  class DeletedTCImage extends TCImage{

    String time_deleted;

    DeletedTCImage(TCImage img){
      uri = img.uri;
      time = img.time;
      width = img.width;
      height = img.height;
      items = img.items;
      stencil_item = img.stencil_item;
      stencil_location = img.stencil_location;
      time_deleted = CurrentTime.getTime();
    }

    JsonObject toJson(int picture_id){
      JsonObject imageJson = new JsonObject();

      // file information
      JsonObject fileJson = new JsonObject();
      String file_name;
      if(uri == null) file_name = "FILE_NOT_FOUND";
      else file_name = uri.getLastPathSegment();
      fileJson.addProperty("name", file_name);
      fileJson.addProperty("height", height);
      fileJson.addProperty("width", width);
      imageJson.add("file", fileJson);

      // time
      imageJson.addProperty("time", time);
      imageJson.addProperty("time_deleted", time_deleted);

      // objects v2
      if(Version.getInstance().getVersion() == 2){
        JsonArray objectsJson = new JsonArray();
        for(String item : items){
          objectsJson.add(item);
        }
        imageJson.add("objects", objectsJson);
      }

      // objects v3
      else if(Version.getInstance().getVersion() == 3){
        int[] location = getStencilLocation();
        JsonObject objectJson = new JsonObject();
        objectJson.addProperty("name", stencil_item);
        objectJson.addProperty("x", location[0]);
        objectJson.addProperty("y", location[1]);
        objectJson.addProperty("width", location[2]);
        objectJson.addProperty("height,", location[3]);
        imageJson.add("object", objectJson);
      }

      // final json
      String name = "deleted_picture_" + Integer.toString(picture_id);
      JsonObject jsonObject = new JsonObject();
      jsonObject.add(name, imageJson);
      return jsonObject;
    }

  }
}
