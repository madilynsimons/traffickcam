package com.tcamdev.traffickcam;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.bumptech.glide.request.target.ViewTarget;
import com.tcamdev.traffickcam.versions.Version;

/**
 * Application class which is the starting point of the app and is created before all activities.
 *
 * Code that must be executed once exactly at startup is placed here. This is ideal
 * compared to placing these in activities since the startup logic could be duplicated if the
 * activity stops and starts repeatedly.
 */
public class App extends Application {
    @Override public void onCreate() {
        super.onCreate();
        ViewTarget.setTagId(R.id.glide_tag);

        // restore previously selected version
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Version.getInstance().setVersion(sharedPreferences.getInt("version",-1));
    }
}