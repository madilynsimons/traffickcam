package com.tcamdev.traffickcam.versions;


import android.graphics.drawable.Drawable;

/** NOTE: Only needed for version 3
 *  Class that describes a stencil object for v3 of the TraffickCam app
 */
public class Stencil {

    private int id;
    private Drawable stencil;
    private int icon;
    private String instructions;
    private int textColor;

    public Stencil(int id, int icon, Drawable stencil, String instructions, int textColor) {
        this.id = id;
        this.icon = icon;
        this.stencil = stencil;
        this.instructions = instructions;
        this.textColor = textColor;
    }

    public int getId() {
        return id;
    }

    public int getIcon() {
        return icon;
    }

    public Drawable getStencil() {
        return stencil;
    }

    public String getInstructions() {
        return instructions;
    }

    public int getColor() {
        return textColor;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStencil(Drawable stencil) {
        this.stencil = stencil;
    }

    public void setInstruction(String instructions) {
        this.instructions = instructions;
    }

    public void setTextColor(int textColor) { this.textColor = textColor; }
}
