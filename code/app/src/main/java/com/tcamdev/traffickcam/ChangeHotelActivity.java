package com.tcamdev.traffickcam;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import com.tcamdev.traffickcam.userstudy.TouchLog;
import com.tcamdev.traffickcam.util.SearchItem;
import com.tcamdev.traffickcam.util.SearchListAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * For more info: https://codinginflow.com/tutorials/android/searchview-recyclerview
 */
public class ChangeHotelActivity extends AppCompatActivity implements OnTouchListener {


  private List<SearchItem> search_items;

  RecyclerView hotel_list_view;
  SearchListAdapter search_list_adapter;

  Intent return_intent;
  EditText search_view;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_change_hotel);

    return_intent = new Intent();
    setResult(Activity.RESULT_CANCELED, return_intent);

    initSearchItems();
    initRecycleView();
    initSearchView();
    enableTouchLogging();
  }

  void enableTouchLogging(){
    findViewById(R.id.hotelSearchList).setOnTouchListener(this);
    findViewById(R.id.searchView).setOnTouchListener(this);
  }

  void initSearchView(){
    search_view = findViewById(R.id.searchView);
    search_view.setImeOptions(EditorInfo.IME_ACTION_DONE);
    search_view.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
      @Override
      public void afterTextChanged(Editable s) {}

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        search_list_adapter.getFilter().filter(s.toString());
      }
    });
  }

  // Get names and addresses to nearby hotels and add them to list of searchable items
  void initSearchItems(){
    ArrayList<String> hotel_names = getIntent().getStringArrayListExtra("hotel_names");
    ArrayList<String> hotel_addresses = getIntent().getStringArrayListExtra("hotel_addresses");
    search_items = new ArrayList<>();
    for(int i = 0; i < hotel_names.size(); i++){
      String name = hotel_names.get(i);
      String address = hotel_addresses.get(i);
      SearchItem hotel = new SearchItem(name, address);
      search_items.add(hotel);
    }
  }

  void initRecycleView(){
    hotel_list_view = findViewById(R.id.hotelSearchList);
    hotel_list_view.setHasFixedSize(true);
    RecyclerView.LayoutManager layout_manager = new LinearLayoutManager(this);
    hotel_list_view.setLayoutManager(layout_manager);
    search_list_adapter = new SearchListAdapter(this, search_items);
    hotel_list_view.setAdapter(search_list_adapter);
  }

  public void setHotel(String hotel_name){
    return_intent.putExtra("hotel_name", hotel_name);
    setResult(Activity.RESULT_OK, return_intent);
    finish();
  }

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    TouchLog.logTouch(this,v, event);
    //if(event.getAction() == MotionEvent.ACTION_UP) v.performClick();
    return false;
  }
}
