package com.tcamdev.traffickcam.versions;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.Gravity;
import android.widget.Toast;
import com.jsibbold.zoomage.ZoomageView;
import com.tcamdev.traffickcam.HotelImagePickerActivity;
import com.tcamdev.traffickcam.R;
import com.tcamdev.traffickcam.view.FotoapparatFragment;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.DiscreteScrollView.OnItemChangedListener;
import com.yarolegovich.discretescrollview.DiscreteScrollView.ScrollStateChangeListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class V3Graphics implements OnItemChangedListener<ViewHolder>,
    ScrollStateChangeListener<ViewHolder> {

  // current activity
  HotelImagePickerActivity activity;

  // camera
  FotoapparatFragment camera;

  // pink frames that the user uses to define where an object is
  List<Stencil> item_boundary_stencils;

  // current stencil being used by the user when using the camera
  Stencil current_item_stencil;

  // scroll view for user to pick which stencil they want to use
  DiscreteScrollView item_stencil_picker;

  // view for stencils (lol)
  ZoomageView stencil_view;

  // display what the user ought to take a picture of
  Toast instructions_toast;

  // location of stencil
  // stencil_location[0] = x position
  // stencil_location[1] = y position
  // stencil_location[2] = width
  // stencil_location[3] = height
  int[] stencil_location = {-1, -1, -1, -1};

  public V3Graphics(HotelImagePickerActivity activity) {
    this.activity = activity;
    initStencils();
    initStencilView();
    instructions_toast = Toast.makeText(activity, "null", Toast.LENGTH_SHORT);
    instructions_toast.setGravity(Gravity.CENTER, 0, 200);
    if(current_item_stencil.getInstructions() != null){
      instructions_toast.setText(current_item_stencil.getInstructions());
      instructions_toast.show();
    }
  }

  @SuppressLint("ClickableViewAccessibility")
  private void initStencilView(){
    stencil_view = activity.findViewById(R.id.stencil_view);
    stencil_view.setImageDrawable(current_item_stencil.getStencil());
  }

  public void addImage(final Uri uri) {
    stencil_location = findStencil(uri);
    activity.getImageData().addImage(uri, current_item_stencil.getInstructions(), stencil_location);
  }

  private int[] findStencil(final Uri uri){

    int[] stencilLocation = {-1, -1, -1, -1};

    Bitmap bitmap;
    try{
      bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      return stencilLocation;
    } catch (IOException e) {
      e.printStackTrace();
      return stencilLocation;
    }

    int x0 = (int)stencil_view.getDrawableX();
    int y0 = (int)stencil_view.getDrawableY();
    int width0 = stencil_view.getWidth();
    int width1 = bitmap.getWidth();

    int y1 = width1 * y0 / width0;
    int x1 = width1 * x0 / width0;

    stencilLocation[0] = x1;
    stencilLocation[1] = y1;
    stencilLocation[2] = (int)stencil_view.getCurrentDisplayedWidth() / 4;
    stencilLocation[3] = (int)stencil_view.getCurrentDisplayedHeight() / 4;

    return stencilLocation;
  }


  void initStencilPicker(FotoapparatFragment cf){
    camera = cf;
    item_stencil_picker = activity.findViewById(R.id.stencil_picker);
  }

  public void initStencils(){
    Resources resources = activity.getResources();
    item_boundary_stencils = getStencils(resources);
    current_item_stencil = item_boundary_stencils.get(0);
  }

  public static List<Stencil> getStencils(Resources resources){

    if(Version.getInstance().getVersion() == 3) {

      Drawable bedDraw = ResourcesCompat
          .getDrawable(resources, R.drawable.stencil_bed_transparent, null);
      Drawable lampDraw = ResourcesCompat
          .getDrawable(resources, R.drawable.stencil_lamp_transparent, null);
      Drawable deskDraw = ResourcesCompat
          .getDrawable(resources, R.drawable.stencil_desk_transparent, null);
      Drawable chairDraw = ResourcesCompat
          .getDrawable(resources, R.drawable.stencil_chair_transparent, null);
      Drawable couchDraw = ResourcesCompat
          .getDrawable(resources, R.drawable.stencil_couch_transparent, null);
      Drawable artworkDraw = ResourcesCompat
          .getDrawable(resources, R.drawable.stencil_art_transparent, null);
      Drawable sinkDraw = ResourcesCompat
          .getDrawable(resources, R.drawable.stencil_sink_transparent, null);
      Drawable toiletDraw = ResourcesCompat
          .getDrawable(resources, R.drawable.stencil_toilet_transparent, null);
      Drawable blankDraw = ResourcesCompat
          .getDrawable(resources, R.drawable.blank_transparent, null);

      int textColor = R.color.colorAccent;

      List<Stencil> ret = Arrays.asList(
          new Stencil(1, R.drawable.img_bed, bedDraw, "Bed", textColor),
          new Stencil(2, R.drawable.img_lamp, lampDraw, "Lamp", textColor),
          new Stencil(3, R.drawable.img_desk, deskDraw, "Desk", textColor),
          new Stencil(4, R.drawable.img_chair, chairDraw, "Chair", textColor),
          new Stencil(5, R.drawable.img_couch, couchDraw, "Couch", textColor),
          new Stencil(6, R.drawable.img_artwork, artworkDraw, "Artwork", textColor),
          new Stencil(7, R.drawable.img_sink, sinkDraw, "Sink", textColor),
          new Stencil(8, R.drawable.img_toilet, toiletDraw, "Toilet", textColor),
          new Stencil(0, R.drawable.blank, blankDraw, "Other", textColor));

      return ret;
    }
    return new ArrayList<>();
  }

  @Override
  public void onCurrentItemChanged(@Nullable ViewHolder viewHolder, int adapterPosition) {
    if(viewHolder != null){

        viewHolder.itemView.findViewById(R.id.hotel_item).setTag("current");
        Stencil stencil = item_boundary_stencils.get(adapterPosition);
        current_item_stencil = stencil;
        stencil_view.setImageDrawable(current_item_stencil.getStencil());

      if(current_item_stencil.getInstructions() != null){
        instructions_toast.setText(current_item_stencil.getInstructions());
        instructions_toast.show();
      }
    }
  }

  @Override
  public void onScrollStart(@NonNull ViewHolder currentItemHolder, int adapterPosition) {
  }

  @Override
  public void onScrollEnd(@NonNull ViewHolder currentItemHolder, int adapterPosition) {
  }

  @Override
  public void onScroll(float scrollPosition, int currentPosition, int newPosition,
      @Nullable ViewHolder currentHolder, @Nullable ViewHolder newCurrent) {
    Stencil current = current_item_stencil;
    if(newPosition >= 0 && newPosition < item_stencil_picker.getAdapter().getItemCount()){
      Stencil next = item_boundary_stencils.get(newPosition);
      if(currentHolder != null)
        currentHolder.itemView.findViewById(R.id.hotel_item).setTag("previous");
    }

  }
}
