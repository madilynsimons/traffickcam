package com.tcamdev.traffickcam.util;

public class SearchItem {

  public String primary_descriptor;
  public String secondary_descriptor;

  private String description;

  public SearchItem(String primary, String secondary){
    primary_descriptor = primary;
    secondary_descriptor = secondary;

    description = primary + " " + secondary;
  }

  public String getDescription(){
    return description;
  }

}
