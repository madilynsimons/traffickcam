package com.tcamdev.traffickcam.versions;

/** NOTE: Only needed for version 3
 ** A Singleton class that states the current active version of the TraffickCam app **/
public class Version {

    private static Version instance;
    private int version;
    private String userID = "USER_ID_NOT_FOUND";

    private Version() {
    }

    public static Version getInstance() {
        if (instance == null)
            instance = new Version();
        return instance;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getUserID() {
        return userID;
    }
    public void setUserID(String userID) {
        this.userID = userID;
    }

}
