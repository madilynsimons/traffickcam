package com.tcamdev.traffickcam.util;

import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CurrentTime {

  public static String getTime(){
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat format = new SimpleDateFormat("HH_mm_ss_SSS");
    Date date = new Date();
    return format.format(date);
  }

  public static String year(){
    int y = Calendar.getInstance().get(Calendar.YEAR);
    return Integer.toString(y);
  }

  public static String month(){
    int m = Calendar.getInstance().get(Calendar.MONTH);
    switch(m){
      case 0:
        return "01_JAN";
      case 1:
        return "02_FEB";
      case 2:
        return "03_MAR";
      case 3:
        return "04_APR";
      case 4:
        return "05_MAY";
      case 5:
        return "06_JUN";
      case 6:
        return "07_JULY";
      case 7:
        return "08_AUG";
      case 8:
        return "09_SEPT";
      case 9:
        return "10_OCT";
      case 10:
        return "11_NOV";
      case 11:
        return "12_DEC";
    }
    return "UNKNOWN_MONTH";
  }

  public static String day() {
    int d = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    return Integer.toString(d);
  }

}
