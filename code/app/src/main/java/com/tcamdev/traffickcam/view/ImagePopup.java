package com.tcamdev.traffickcam.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.tcamdev.traffickcam.HotelImagePickerActivity;
import com.tcamdev.traffickcam.R;
import com.tcamdev.traffickcam.userstudy.TouchLog;
import com.tcamdev.traffickcam.versions.Version;

public class ImagePopup{

  private static Activity mActivity;

  @SuppressLint("ClickableViewAccessibility")
  public static void show(final HotelImagePickerActivity activity, final Uri uri) {

    mActivity = activity;

    AlertDialog.Builder builder = new Builder(activity);
    LayoutInflater inflater = activity.getLayoutInflater();
    View view = inflater.inflate(R.layout.image_pop_up_v1, null);
    builder.setView(view);
    final Dialog dialog = builder.create();

    ImageView imageView = view.findViewById(R.id.image);
    imageView.setImageURI(uri);

    TextView textView = view.findViewById(R.id.text);
    if(Version.getInstance().getVersion() == 3){
      String item = activity.getImageData().getImage(uri).getStencilItem();
      String msg = "Image with a(n) " + item.toLowerCase() ;
      textView.setText(msg);
    }else{
      textView.setText("");
    }

    ImageButton close = view.findViewById(R.id.close);
    close.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
      }
    });

    Button cancel_btn = view.findViewById(R.id.cancel_btn);
    cancel_btn.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
      }
    });

    Button delete_picture_btn = view.findViewById(R.id.delete_picture_btn);
    delete_picture_btn.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        AlertDialog.Builder builder = new Builder(activity);
        builder.setMessage("Are you sure you want to delete this picture?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface d, int which) {
            d.dismiss();
            dialog.dismiss();
            activity.removeImage(uri);
          }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface d, int which) {
            d.dismiss();
          }
        });
        Dialog child_dialog = builder.create();
        child_dialog.show();
      }
    });

    dialog.show();
    dialog.setCancelable(true);

    delete_picture_btn.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        return onTouchIP(v, event);
      }
    });
    cancel_btn.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        return onTouchIP(v, event);
      }
    });
    close.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        return onTouchIP(v, event);
      }
    });

  }

  public static boolean onTouchIP(View v, MotionEvent event) {
    TouchLog.logTouch(mActivity, v, event);
    //if(event.getAction() == MotionEvent.ACTION_UP) v.performClick();
    return false;
  }
}
