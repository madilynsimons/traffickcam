package com.tcamdev.traffickcam.view;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.tcamdev.traffickcam.HotelImagePickerActivity;
import com.tcamdev.traffickcam.R;
import com.tcamdev.traffickcam.userstudy.TouchLog;
import com.tcamdev.traffickcam.versions.StencilAdapter;
import com.tcamdev.traffickcam.versions.V3Graphics;
import com.tcamdev.traffickcam.versions.Version;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;
import id.zelory.compressor.Compressor;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.result.BitmapPhoto;
import io.fotoapparat.result.PendingResult;
import io.fotoapparat.result.PhotoResult;
import io.fotoapparat.result.WhenDoneListener;
import io.fotoapparat.view.CameraView;
import java.io.File;
import org.jetbrains.annotations.Nullable;

public class FotoapparatFragment extends Fragment implements View.OnClickListener, OnTouchListener {

  HotelImagePickerActivity activity;
  boolean is_processing;
  File mFile;
  Fotoapparat mCamera;
  ProgressBar progressBar;
  CameraView cameraView;
  View view;

  public static int CAMERA_HEIGHT;
  public static int CAMERA_WIDTH;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fotoapparat_fragment, container, false);
  }

  @Override
  public void onViewCreated(final View view, Bundle savedInstanceState) {
      this.view = view;
      view.findViewById(R.id.btn_take_picture).setOnClickListener(this);
      activity = (HotelImagePickerActivity) getActivity();
      is_processing = false;
      cameraView = view.findViewById(R.id.camera_view);

      mCamera = new Fotoapparat(activity, cameraView);
      mCamera.start();
      activity.setCamera(FotoapparatFragment.this);
      Resources resources = activity.getResources();

      if(Version.getInstance().getVersion() == 3){
        DiscreteScrollView item_stencil_picker = view.findViewById(R.id.stencil_picker);
        item_stencil_picker.setAdapter(
            new StencilAdapter(V3Graphics.getStencils(resources), FotoapparatFragment.this));
        item_stencil_picker.addOnItemChangedListener((V3Graphics) activity.tcGraphics.getGraphics());
        item_stencil_picker.addScrollStateChangeListener((V3Graphics) activity.tcGraphics.getGraphics());
        item_stencil_picker.setSlideOnFling(true);
        item_stencil_picker.setItemTransitionTimeMillis(150);
        item_stencil_picker.setItemTransformer(new ScaleTransformer.Builder()
            .setMinScale(0.8f)
            .build());
      }
      progressBar = activity.findViewById(R.id.progressBar);
      progressBar.bringToFront();
      progressBar.setVisibility(View.INVISIBLE);

      enableTouchLogging();
    }

   void enableTouchLogging(){
    view.findViewById(R.id.btn_take_picture).setOnTouchListener(this);
    if(Version.getInstance().getVersion() == 3){
      view.findViewById(R.id.stencil_picker).setOnTouchListener(this);
    }
   }

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    TouchLog.logTouch(activity, v, event);
    //if(event.getAction() == MotionEvent.ACTION_UP) v.performClick();
    return false;
  }

  @Override
  public void onClick(View v) {

      if((v.getId() == R.id.btn_take_picture && Version.getInstance().getVersion() != 3)
      || (v.getTag() == "current" && Version.getInstance().getVersion() == 3)) {

        CAMERA_HEIGHT = cameraView.getHeight();
        CAMERA_WIDTH = cameraView.getWidth();

        if (!is_processing) {
          is_processing = true;
          progressBar.setVisibility(View.VISIBLE);
          initImageFile();
          takePicture();
        }
      }
  }

  void initImageFile(){
    mFile = new File(getActivity().getExternalFilesDir(null), System.currentTimeMillis()+"_capture.jpg");
    mFile.deleteOnExit();
  }

  void takePicture(){
    PhotoResult photoResult = mCamera.takePicture();
    PendingResult<BitmapPhoto> pendingResult = photoResult.toBitmap();
    pendingResult.whenDone(new WhenDoneListener<BitmapPhoto>() {
      @Override
      public void whenDone(@Nullable BitmapPhoto bitmapPhoto) {
        Bitmap ret = bitmapPhoto.bitmap;
        UploadPhoto thread = new UploadPhoto(ret);
        thread.run();
      }

    });
  }

  class UploadPhoto extends Thread{
    Bitmap ret;
    public UploadPhoto(Bitmap bitmap){
      ret = bitmap;
    }
    public void run(){
      try{

        is_processing = false;
        progressBar.setVisibility(View.INVISIBLE);


        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap scaledBitmap
            = Bitmap.createScaledBitmap(ret, ret.getWidth()/12, ret.getHeight()/12, false);
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, false);

        int width = rotatedBitmap.getWidth();
        int height = CAMERA_HEIGHT * width / CAMERA_WIDTH;
   /*
        int dy = (rotatedBitmap.getHeight() - height)/2;


        Bitmap cropped = Bitmap.createBitmap(rotatedBitmap, 0,dy,width,height);

        try{
          FileOutputStream out = new FileOutputStream(mFile);
          cropped.compress(CompressFormat.JPEG, 75, out);
          out.flush();
          out.close();
        }catch(Exception e){
          e.printStackTrace();
        }
        */

        Compressor compressor = new Compressor(activity)
            .setMaxWidth(width)
            .setMaxHeight(height)
            .setQuality(50)
            .setCompressFormat(Bitmap.CompressFormat.JPEG);

        compressor.compressToFile(mFile);

        activity.addImage(Uri.fromFile(mFile));

      }catch(Exception e){
        e.printStackTrace();
      }

    }
  }

  public void start(){
    mCamera.start();
  }

  public void stop(){
    mCamera.stop();
  }
}
