package com.tcamdev.traffickcam;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.tcamdev.traffickcam.versions.Version;


public class VersionPickerActivity extends Activity {

    Context activity = this;
    private Button startButton;
    private EditText userIDEditText;
    public final static String activity_tag = "VERSION_PICKER_ACTIVITY";

    /**
     * @param savedInstanceState Called when TraffickCam logo is clicked 3 times. This is to
     *                           prevent participants of the user study from changing versions
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.version_picker);

        userIDEditText = findViewById(R.id.userID);
        initStartButton();
    }

    private void initStartButton(){
        startButton = findViewById(R.id.start_btn);
        startButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(userIDEditText.getText())){
                    AlertDialog.Builder builder = new Builder(activity);
                    builder.setMessage("Please enter user ID before continuing");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.create().show();
                }else{
                    String user_id_str = userIDEditText.getText().toString();
                    Version.getInstance().setUserID(user_id_str);

                    int user_id = Integer.parseInt(user_id_str);
                    int version = user_id % 3 + 1;
                    Version.getInstance().setVersion(version);

                    Intent intent = new Intent(getApplicationContext(), TutorialActivity.class);
                    intent.putExtra("parent", activity_tag);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    /**
     * Save state of radio buttons and the current version
     */
    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor shEditor = sharedPreferences.edit();
        shEditor.putInt("version", Version.getInstance().getVersion());
        shEditor.apply();
    }

}