package com.tcamdev.traffickcam;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tcamdev.traffickcam.places.Hotel;
import com.tcamdev.traffickcam.places.HotelList;
import com.tcamdev.traffickcam.places.UserHotel;
import com.tcamdev.traffickcam.userstudy.Logger;
import com.tcamdev.traffickcam.userstudy.TouchLog;
import java.util.ArrayList;

public class SubmitDataActivity extends AppCompatActivity implements OnTouchListener {

  // activity data
  public static final int NEW_HOTEL = 100;

  // views
 // ImageView imagePopupView;
  //AlertDialog imagePopup;
  ProgressBar progressBar;
  AlertDialog uploadingImagesDialog;
  TextView hotelAddressText;
  EditText hotelNumberText;
  Button submitInfoButton;
  Button editHotelButton;
  ViewGroup selectedItemsContainer;
  final int LOADING_ACTIVITY_ID = 101;
  boolean loading_activity_running = false;
  boolean loading_activity_cancelled_by_user = true;

  // user data
  Hotel hotel_data = null;
  ArrayList<Uri> image_uris = null;
  ArrayList<Uri> deleted_image_uri = null;

  // JSONs
  String images_json = "";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    getWindow().setSoftInputMode(
        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    if(!HotelList.saved)
      hotel_data = new Hotel(this);

    selectedItemsContainer = findViewById(R.id.selected_photos_container);

    image_uris = getIntent().getParcelableArrayListExtra(HotelImagePickerActivity.ADDED_IMAGES_URI);
    deleted_image_uri = getIntent().getParcelableArrayListExtra(HotelImagePickerActivity.DELETED_IMAGES_URI);
    images_json = getIntent().getStringExtra(HotelImagePickerActivity.IMAGES_JSON);
    Log.d("PHOTOS_TRANSFERRED", Integer.toString(image_uris.size()));
    if (image_uris != null) showImages();

    initButtons();
    progressBar = findViewById(R.id.HotelAddProgressBar);

    hotelAddressText = findViewById(R.id.HotelAddressText);
    hotelAddressText.setVisibility(View.INVISIBLE);

    if(!HotelList.saved){
      progressBar.setVisibility(View.VISIBLE);
    }else{
      setHotelData(HotelList.names, HotelList.addresses);
    }
    enableTouchLogging();
  }

  public void setHotelData(ArrayList<String> names, ArrayList<String> addresses){
    if(names != null){
      HotelList.saved = true;

      if(names.size() > 0){
        if(HotelList.userHotel == null){
          HotelList.userHotel = new UserHotel(names.get(0), addresses.get(0));
        }
        HotelList.names = names;
        HotelList.addresses = addresses;
        progressBar.setVisibility(View.INVISIBLE);
        hotelAddressText.setText(HotelList.userHotel.name());
        hotelAddressText.setVisibility(View.VISIBLE);
        if(loading_activity_running) {
          finishActivity(LOADING_ACTIVITY_ID);
          loading_activity_running = false;
          loading_activity_cancelled_by_user = false;
        }
      }
    }
  }

  private void showImages() {
    // Remove all views before
    // adding the new ones.
    selectedItemsContainer.removeAllViews();
    if (image_uris.size() >= 1) {
      selectedItemsContainer.setVisibility(View.VISIBLE);
    }

    final int wdpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
    final int htpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());


    for (final Uri uri : image_uris) {

      View imageHolder = LayoutInflater.from(this).inflate(R.layout.image_item, null);
      ImageView thumbnail = (ImageView) imageHolder.findViewById(R.id.media_image);

      Glide.with(this)
        .load(uri.toString())
        .apply(new RequestOptions()
        .fitCenter())
        .into(thumbnail);

      selectedItemsContainer.addView(imageHolder);

      final Activity activity = this;
      thumbnail.setLayoutParams(new FrameLayout.LayoutParams(wdpx, htpx));
      thumbnail.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
        }
      });
    }
  }

  void initButtons()
  {
    submitInfoButton = findViewById(R.id.SubmitInfoButton);
    editHotelButton = findViewById(R.id.ChangeHotelButton);
    hotelNumberText = findViewById(R.id.HotelNumberText);
    hotelNumberText.setHint(getResources().getString(R.string.room_num_optional));

      submitInfoButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // check if hotel name is populated. If it is, show success message
        if (HotelList.userHotel == null){
          // TODO
          String msg = "Please select your hotel using \"Change Hotel\"";
          Toast.makeText(SubmitDataActivity.this, msg, Toast.LENGTH_SHORT).show();
        }
        else{
          if(hotelNumberText.getText() != null){
            String name = HotelList.userHotel.name();
            String address = HotelList.userHotel.address();
            String room = hotelNumberText.getText().toString();
            HotelList.userHotel = new UserHotel(name, address, room);
          }
          uploadData();
        }
      }
    });

    editHotelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showHotels();
      }
    });
  }

  void showHotels(){
    if(HotelList.userHotel == null){
      loading_activity_running = true;
      Intent intent = new Intent(SubmitDataActivity.this, LoadingHotelActivity.class);
      startActivityForResult(intent, LOADING_ACTIVITY_ID);
    } else if(HotelList.noHotelsFound) {
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      builder.setTitle("No nearby hotels found...");
      builder.setMessage("It doesn't look like you're near any hotels.  Please check your internet connection.");
      builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
            }
          });
      AlertDialog dialog = builder.create();
      dialog.show();
    } else
     {
      Intent intent = new Intent(SubmitDataActivity.this, ChangeHotelActivity.class);
      Bundle bundle = new Bundle();
      bundle.putStringArrayList("hotel_names", HotelList.names);
      bundle.putStringArrayList("hotel_addresses", HotelList.addresses);
      intent.putExtras(bundle);
      startActivityForResult(intent, NEW_HOTEL);
    }
  }

  void uploadData(){
    Logger.uploadData(this,
        images_json,
        image_uris,
        deleted_image_uri,
        HotelList.userHotel);
  }

  void cancelUpload(){
    // TODO
  }

  public void finishUploadSuccess(){
    exit();
  }

  void finishUploadFailure(){
    if(uploadingImagesDialog == null) return;
    uploadingImagesDialog.cancel();
    Dialog dialog = makeDialog("Upload failed :(");
    dialog.show();
  }

  Dialog makeDialog(String msg){
    AlertDialog.Builder builder = new AlertDialog.Builder(SubmitDataActivity.this);
    builder.setTitle(msg);
    builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    return builder.create();
  }

  void exit()
  {
    Intent ret = new Intent();
    ret.putExtra("finished", 1);
    setResult(Activity.RESULT_OK, ret);
    finish();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == NEW_HOTEL) {
      if(resultCode == Activity.RESULT_OK)
      {
        hotelAddressText.setVisibility(View.VISIBLE);
        String user_hotel_name = data.getStringExtra("hotel_name");
        int i = HotelList.names.indexOf(user_hotel_name);
        String user_hotel_address = HotelList.addresses.get(i);
        hotelAddressText.setText(user_hotel_name);
        HotelList.userHotel = new UserHotel(user_hotel_name, user_hotel_address);
      }
    } else if(requestCode == LOADING_ACTIVITY_ID){
      if(!loading_activity_cancelled_by_user){
        loading_activity_running = false;
        showHotels();
      }
    }
  }

  void enableTouchLogging(){
    findViewById(R.id.hori_scroll_view).setOnTouchListener(this);
    findViewById(R.id.ChangeHotelButton).setOnTouchListener(this);
    findViewById(R.id.SubmitInfoButton).setOnTouchListener(this);
    findViewById(R.id.HotelNumberText).setOnTouchListener(this);
  }

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    TouchLog.logTouch(this, v, event);
    return false;
  }
}
