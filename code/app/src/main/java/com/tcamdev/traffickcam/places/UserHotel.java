package com.tcamdev.traffickcam.places;

public class UserHotel {

  private String name = "UNKNOWN_HOTEL_NAME";
  private String address = "UNKNOWN_HOTEL_ADDRESS";
  private String room_number = "UNKNOWN_ROOM_NUMBER";
  public UserHotel(String name, String address){
    this.name = name;
    this.address = address;
  }
  public UserHotel(String name, String address, String room_number){
    this.name = name;
    this.address = address;
    this.room_number = room_number;
  }
  public String name(){ return name; }
  public String address(){ return address;}
  public String roomNumber(){ return room_number; }
}
