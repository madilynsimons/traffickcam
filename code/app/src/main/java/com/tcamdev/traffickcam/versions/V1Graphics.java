package com.tcamdev.traffickcam.versions;

import android.net.Uri;
import com.tcamdev.traffickcam.HotelImagePickerActivity;

public class V1Graphics {

  HotelImagePickerActivity activity;

  V1Graphics(HotelImagePickerActivity activity){ this.activity = activity;}

  void addImage(final Uri uri){
    activity.getImageData().addImage(uri);
  }

}
